package com.tora;

import com.tora.peer_to_peer.PeerThread;
import com.tora.peer_to_peer.ServerThread;

import javax.json.Json;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.Socket;


public class App 
{
    public static void main( String[] args ) throws Exception
    {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter username: ");
        String username = bufferedReader.readLine();
        System.out.println("Enter port: ");
        String port = bufferedReader.readLine();
        ServerThread serverThread = new ServerThread(port);
        serverThread.start();
        new App().updateListenToPeers(bufferedReader,username,serverThread);
    }
    public void updateListenToPeers(BufferedReader reader, String usrn, ServerThread server) throws Exception {
        System.out.println("Enter peers to receive messages from:");
        System.out.println("Format: <hostname:port>, separated by space:");
        String line = reader.readLine();
        String[] peers = line.split(" ");
        if(!line.equals(""))
            for(int i=0 ; i<peers.length ; i++) {
                String[] pair = peers[i].split(":");
                Socket socket = null;
                try{
                    socket = new Socket(pair[0], Integer.parseInt(pair[1]));
                    new PeerThread(socket).start();
                } catch (Exception e){
                    if(socket!=null)
                        socket.close();
                    else
                        System.out.println("Invalid input!");
                }
            }
        communicate(reader,usrn,server);
    }

    public void communicate(BufferedReader reader, String usrn, ServerThread server) {
        try {
            System.out.println("line of communication open!");
            System.out.println("e - exit, c - change");
            boolean flag = true;
            while(flag) {
                String message = reader.readLine();
                if(message.equals("e")) {
                    flag = false;
                    break;
                }
                else if (message.equals("c")) {
                    updateListenToPeers(reader,usrn,server);
                } else {
                    StringWriter stringWriter = new StringWriter();
                    Json.createWriter(stringWriter).writeObject(Json.createObjectBuilder().add("username",usrn).add("message",message).build());
                    server.sendMessage(stringWriter.toString());
                }
            }
            System.exit(0);
        } catch (Exception e) {}
    }
}
