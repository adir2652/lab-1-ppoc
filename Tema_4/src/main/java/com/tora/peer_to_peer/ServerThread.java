package com.tora.peer_to_peer;

import java.net.ServerSocket;
import java.util.Set;
import java.util.HashSet;
import java.io.IOException;

public class ServerThread extends Thread {
    private ServerSocket serverSocket;
    private Set<ServerThreadThread> serverThreadThreads = new HashSet<ServerThreadThread>();
    public ServerThread(String port) throws IOException {
        serverSocket = new ServerSocket(Integer.parseInt(port));

    }
    public void run() {
        try {
            while(true){
                ServerThreadThread serverThreadThread = new ServerThreadThread(serverSocket.accept(),this);
                serverThreadThreads.add(serverThreadThread);
                serverThreadThread.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void sendMessage(String message){
        try {
            serverThreadThreads.forEach(t->t.getPrintWriter().println(message));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Set<ServerThreadThread> getServerThreadThreads() {
        return serverThreadThreads;
    }
}