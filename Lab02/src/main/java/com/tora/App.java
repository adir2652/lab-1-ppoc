package com.tora;

import org.openjdk.jmh.runner.RunnerException;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws RunnerException, IOException {
        org.openjdk.jmh.Main.main(args);
    }
}
