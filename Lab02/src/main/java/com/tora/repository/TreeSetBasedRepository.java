package com.tora.repository;

import com.tora.entities.Order;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository implements InMemoryRepository<Order>{

    private final Set<Order> set;

    public TreeSetBasedRepository(){
        set = new TreeSet<Order>();
    }

    @Override
    public void add(Order element) {
        set.add(element);
    }

    @Override
    public boolean contains(Order element) {
        for(Order o : set){
            if(o.equals(element))
                return true;
        }
        return false;
    }

    @Override
    public void remove(Order element) {
        if(contains(element))
            set.remove(element);
    }
}
