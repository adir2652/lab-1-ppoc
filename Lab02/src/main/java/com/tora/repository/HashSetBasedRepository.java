package com.tora.repository;

import com.tora.entities.Order;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository implements InMemoryRepository<Order>{

    public final Set<Order> set;

    public HashSetBasedRepository(){
        set = new HashSet<Order>();
    }

    @Override
    public void add(Order element) {
        set.add(element);
    }

    @Override
    public boolean contains(Order element) {
        for(Order o : set) {
            if (o.equals(element))
                return true;
        }
        return false;
    }

    @Override
    public void remove(Order element) {
        if(contains(element))
            set.remove(element);
    }
}
