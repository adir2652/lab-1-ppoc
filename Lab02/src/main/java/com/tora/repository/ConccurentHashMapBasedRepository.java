package com.tora.repository;

import com.tora.entities.Order;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConccurentHashMapBasedRepository implements InMemoryRepository<Order>{

    private final Map<Integer, Order> map;

    public ConccurentHashMapBasedRepository(){
        map = new ConcurrentHashMap<Integer, Order>();
    }

    @Override
    public void add(Order element) {
        map.putIfAbsent(element.getId(),element);
    }

    @Override
    public boolean contains(Order element) {
        Order obj = map.get(element.getId());
        return obj.equals(element);
    }

    @Override
    public void remove(Order element) {
        if(contains(element))
            map.remove(element.getId());
    }
}
