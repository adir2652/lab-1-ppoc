package com.tora.repository;

import com.tora.entities.Order;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository implements InMemoryRepository<Order>{

    private final List<Order> list;

    public ArrayListBasedRepository() {
        list = new ArrayList<>();
    }

    @Override
    public void add(Order element) {
        if(!contains(element))
            list.add(element);
    }

    @Override
    public boolean contains(Order element) {
        for(Order o : list){
            if(o.equals(element))
                return true;
        }
        return false;
    }

    @Override
    public void remove(Order element) {
        if(contains(element))
            list.remove(element);
    }
}
