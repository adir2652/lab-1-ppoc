package com.tora.repository;

import com.tora.entities.Order;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;

import java.util.List;

public class EclipseBasedRepository implements InMemoryRepository<Order>{

    private final MutableList<Order> list;

    public EclipseBasedRepository(){
        list = Lists.mutable.empty();
    }

    @Override
    public void add(Order element) {
        if(!contains(element))
            list.with(element);
    }

    @Override
    public boolean contains(Order element) {
        for(Order o : list){
            if(o.equals(element))
                return true;
        }
        return false;
    }

    @Override
    public void remove(Order element) {
        if(contains(element))
            list.without(element);
    }
}
