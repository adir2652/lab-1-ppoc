package com.tora.benchmark;

import com.tora.entities.Order;
import com.tora.repository.*;
import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestBM {

    @State(Scope.Thread)
    public static class MyState {
        public InMemoryRepository<Order> repo1;
        public InMemoryRepository<Order> repo2;
        public InMemoryRepository<Order> repo3;
        public InMemoryRepository<Order> repo4;
        public InMemoryRepository<Order> repo5;
        public List<Order> elements;

        @Setup(Level.Trial)
        public void doSetup() {
            repo1 = new ArrayListBasedRepository();
            repo2 = new ConccurentHashMapBasedRepository();
            repo3 = new EclipseBasedRepository();
            repo4 = new HashSetBasedRepository();
            repo5 = new TreeSetBasedRepository();
            elements = new ArrayList<>();
            elements.add(new Order(1, 27, 10));
            elements.add(new Order(2, 9, 16));
            elements.add(new Order(3, 10, 55));
            elements.add(new Order(4, 8, 17));
            elements.add(new Order(1, 27, 10));
        }
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @Fork(value = 1)
    @Warmup(iterations = 2)
    @Measurement(iterations = 2)
    public void TestArrayListAdd(MyState state) {
        for (Order o : state.elements)
            state.repo1.add(o);
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @Fork(value = 1)
    @Warmup(iterations = 2)
    @Measurement(iterations = 2)
    public void TestConcHashMapAdd(MyState state) {
        for (Order o : state.elements)
            state.repo2.add(o);
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @Fork(value = 1)
    @Warmup(iterations = 2)
    @Measurement(iterations = 2)
    public void TestEclipseAdd(MyState state) {
        for (Order o : state.elements)
            state.repo3.add(o);
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @Fork(value = 1)
    @Warmup(iterations = 2)
    @Measurement(iterations = 2)
    public void TestHashSetAdd(MyState state) {
        for (Order o : state.elements)
            state.repo4.add(o);
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @Fork(value = 1)
    @Warmup(iterations = 2)
    @Measurement(iterations = 2)
    public void TestTreeSetAdd(MyState state) {
        for (Order o : state.elements)
            state.repo5.add(o);
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @Fork(value = 1)
    @Warmup(iterations = 2)
    @Measurement(iterations = 2)
    public void TestArrayListRemove(MyState state) {
        for (Order o : state.elements)
            state.repo1.remove(o);
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @Fork(value = 1)
    @Warmup(iterations = 2)
    @Measurement(iterations = 2)
    public void TestConcHashMapRemove(MyState state) {
        for (Order o : state.elements)
            state.repo2.remove(o);
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @Fork(value = 1)
    @Warmup(iterations = 2)
    @Measurement(iterations = 2)
    public void TestEclipseRemove(MyState state) {
        for (Order o : state.elements)
            state.repo3.remove(o);
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @Fork(value = 1)
    @Warmup(iterations = 2)
    @Measurement(iterations = 2)
    public void TestHashSetRemove(MyState state) {
        for (Order o : state.elements)
            state.repo4.remove(o);
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @Fork(value = 1)
    @Warmup(iterations = 2)
    @Measurement(iterations = 2)
    public void TestTreeSetRemove(MyState state) {
        for (Order o : state.elements)
            state.repo5.remove(o);
    }
}
