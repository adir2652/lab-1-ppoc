package com.tora.entities;

public class Order implements Comparable<Order> {
    private final int id;
    private final int price;
    private final int quantity;

    public Order(int id, int price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    public int getId(){
        return id;
    }

    @Override
    public boolean equals(Object obj){
        if(this == obj)
            return true;
        if(obj == null)
            return false;
        if(this.getClass() != obj.getClass())
            return false;
        Order cast = (Order) obj;
        return id == cast.id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash += 19 * id;
        hash += 29 * price;
        hash += 37 * quantity;
        return hash;
    }

    @Override
    public String toString(){
        String rez = "";
        rez += "Order nr. " + String.valueOf(id);
        rez += " price: " + String.valueOf(price);
        rez += " quantity: " + String.valueOf(quantity);
        return rez;
    }

    @Override
    public int compareTo(Order order){
        int rez = Integer.compare(this.id,order.id);
        if(rez==0)
            return rez;
        rez = Integer.compare(this.price,order.price);
        if(rez==0)
            return rez;
        return Integer.compare(this.quantity,order.quantity);
    }
}
