package com.tora;

import java.math.BigDecimal;
import java.util.Scanner;
import com.tora.repository.Repository;

/**
 * Hello world!
 *
 */
public class App 
{
    public static String generateNumbers(){
        StringBuilder numbers = new StringBuilder();
        for(int i=0 ; i<100 ; i++){
            if(i==0)
                numbers.append(BigDecimal.valueOf(Math.random()*100).toString());
            else
                numbers.append(",").append(BigDecimal.valueOf(Math.random()*100).toString());
        }
        return numbers.toString();
    }

    private static void menu(){
        System.out.println("1 - Print the sum of all numbers");
        System.out.println("2 - Print the average of all numbers");
        System.out.println("3 - Print the Top 10% biggest numbers");
        System.out.println("X - Exit");
    }

    public static void main( String[] args )
    {
        String list = generateNumbers();
        System.out.println("Your list:\n"+list+"\n");
        while(true){
            menu();
            System.out.print("Your command: ");

            Scanner in = new Scanner(System.in);
            String cmd = in.nextLine();

            if(cmd.equals("1")){
                String rez = Repository.sum(list);
                System.out.println("Sum: "+rez);
            }
            else if(cmd.equals("2")){
                String rez = Repository.average(list);
                System.out.println("Average: "+rez);
            }
            else if(cmd.equals("3")){
                String rez = Repository.Top10(list);
                System.out.println("Top 10%:\n"+rez);
            }
            else if(cmd.equals("X")){
                break;
            }
            else{
                System.out.println("INVALID COMMAND!");
            }
            System.out.println("\n");
        }
    }
}
