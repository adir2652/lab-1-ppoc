package com.tora.repository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Repository {
    public static String sum(String list) {
        String[] aux = list.split(",");
        List<BigDecimal> listDecimal = new ArrayList<>();
        for (String s : aux) {
            listDecimal.add(BigDecimal.valueOf(Double.parseDouble(s)));
        }
        return listDecimal.stream().reduce(BigDecimal.ZERO,BigDecimal::add).toString();

    }
    public static String average(String list){
        String[] aux = list.split(",");
        List<BigDecimal> listDecimal = new ArrayList<>();
        for (String s : aux) {
            listDecimal.add(BigDecimal.valueOf(Double.parseDouble(s)));
        }
        BigDecimal sum = listDecimal.stream().reduce(BigDecimal.ZERO,BigDecimal::add);
        return sum.divide(new BigDecimal(listDecimal.size()),17, RoundingMode.HALF_UP).toString();
    }

    public static String Top10(String list){
        String[] listString = list.split(",");
        List<BigDecimal> listDecimal = new ArrayList<>();
        for (String s : listString) {
            listDecimal.add(BigDecimal.valueOf(Double.parseDouble(s)));
        }
        List<BigDecimal> aux = listDecimal.stream().sorted(Comparator.reverseOrder()).limit(listDecimal.size()/10).collect(Collectors.toList());
        StringBuilder rez= new StringBuilder();
        for(BigDecimal element : aux){
            if(rez.length()==0)
                rez.append(element.toString());
            else {
                rez.append(",").append(element.toString());
            }
        }
        return rez.toString();
    }
}
