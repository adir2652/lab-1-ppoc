package com.tora;

import com.tora.repository.Repository;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.tora.App.generateNumbers;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test for simple App.
 */
public class AppTest
{
    @Test
    public void testGenerate(){
        String numbers = generateNumbers();
        String[] listString = numbers.split(",");
        List<BigDecimal> listDecimal = new ArrayList<>();
        for(String s : listString)
            listDecimal.add(BigDecimal.valueOf(Double.parseDouble(s)));
        assertEquals(100,listString.length);
        assertEquals(100,listDecimal.size());
        for(int i=0 ; i<100 ; i++){
            assertEquals(listDecimal.get(i), BigDecimal.valueOf(Double.parseDouble(listString[i])));
        }
    }

    @Test
    public void testAdd1(){
        List<BigDecimal> numbers = new ArrayList<>();
        numbers.add(BigDecimal.valueOf(17.41));
        numbers.add(BigDecimal.valueOf(10.098));
        numbers.add(BigDecimal.valueOf(-8));
        StringBuilder list = new StringBuilder();
        for(BigDecimal element : numbers){
            if(list.length()==0)
                list.append(element.toString());
            else
                list.append(",").append(element.toString());
        }
        String rez = Repository.sum(list.toString());
        BigDecimal value = BigDecimal.valueOf(Double.parseDouble(rez));
        assertEquals(BigDecimal.valueOf(19.508),value);
    }

    @Test
    public void testAdd2(){
        List<BigDecimal> numbers = new ArrayList<>();
        numbers.add(BigDecimal.valueOf(0.962));
        numbers.add(BigDecimal.valueOf(1.071));
        numbers.add(BigDecimal.valueOf(107.204));
        StringBuilder list = new StringBuilder();
        for(BigDecimal element : numbers){
            if(list.length()==0)
                list.append(element.toString());
            else
                list.append(",").append(element.toString());
        }
        String rez = Repository.sum(list.toString());
        BigDecimal value = BigDecimal.valueOf(Double.parseDouble(rez));
        assertEquals(BigDecimal.valueOf(109.237),value);
    }

    @Test
    public void testAverage1(){
        List<BigDecimal> numbers = new ArrayList<>();
        numbers.add(BigDecimal.valueOf(13.08));
        numbers.add(BigDecimal.valueOf(-5.706));
        numbers.add(BigDecimal.valueOf(-4.1111));
        numbers.add(BigDecimal.valueOf(1.205));
        StringBuilder list = new StringBuilder();
        for(BigDecimal element : numbers){
            if(list.length()==0)
                list.append(element.toString());
            else
                list.append(",").append(element.toString());
        }
        String rez = Repository.average(list.toString());
        BigDecimal value = BigDecimal.valueOf(Double.parseDouble(rez));
        assertEquals(BigDecimal.valueOf(1.116975),value);
    }

    @Test
    public void testAverage2(){
        List<BigDecimal> numbers = new ArrayList<>();
        numbers.add(BigDecimal.valueOf(5.172));
        numbers.add(BigDecimal.valueOf(4.04));
        numbers.add(BigDecimal.valueOf(-4.04));
        numbers.add(BigDecimal.valueOf(-5.172));
        StringBuilder list = new StringBuilder();
        for(BigDecimal element : numbers){
            if(list.length()==0)
                list.append(element.toString());
            else
                list.append(",").append(element.toString());
        }
        String rez = Repository.average(list.toString());
        BigDecimal value = BigDecimal.valueOf(Double.parseDouble(rez));
        assertEquals(BigDecimal.valueOf(0.0),value);
    }

    @Test
    public void testTop1(){
        List<BigDecimal> numbers = new ArrayList<>();
        numbers.add(BigDecimal.valueOf(309.10));
        numbers.add(BigDecimal.valueOf(27.087));
        numbers.add(BigDecimal.valueOf(5.1));
        numbers.add(BigDecimal.valueOf(30.12098));
        numbers.add(BigDecimal.valueOf(4.11));
        numbers.add(BigDecimal.valueOf(-6.15));
        numbers.add(BigDecimal.valueOf(416));
        numbers.add(BigDecimal.valueOf(33.78));
        numbers.add(BigDecimal.valueOf(1.09));
        numbers.add(BigDecimal.valueOf(1.09));
        StringBuilder list = new StringBuilder();
        for(BigDecimal element : numbers){
            if(list.length()==0)
                list.append(element.toString());
            else
                list.append(",").append(element.toString());
        }
        String rez = Repository.Top10(list.toString());
        String[] rezString = rez.split(",");
        List<BigDecimal> rezArray = new ArrayList<>();
        for(String s : rezString)
            rezArray.add(BigDecimal.valueOf(Double.parseDouble(s)));
        assertEquals(1,rezArray.size());
        assertEquals(1,rezString.length);
        assertEquals(BigDecimal.valueOf(416.0),rezArray.get(0));
    }

    @Test
    public void testTop2(){
        List<BigDecimal> numbers = new ArrayList<>();
        numbers.add(BigDecimal.valueOf(309.10));
        numbers.add(BigDecimal.valueOf(27.087));
        numbers.add(BigDecimal.valueOf(5.1));
        numbers.add(BigDecimal.valueOf(30.12098));
        numbers.add(BigDecimal.valueOf(4.11));
        numbers.add(BigDecimal.valueOf(-6.15));
        numbers.add(BigDecimal.valueOf(416));
        numbers.add(BigDecimal.valueOf(33.78));
        numbers.add(BigDecimal.valueOf(1.09));
        StringBuilder list = new StringBuilder();
        for(BigDecimal element : numbers){
            if(list.length()==0) {
                list.append(element.toString());
            }
            else
                list.append(",").append(element.toString());
        }
        String rez = Repository.Top10(list.toString());
        assertEquals(0,rez.length());
    }
}
