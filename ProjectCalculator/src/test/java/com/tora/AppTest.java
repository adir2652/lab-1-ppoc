package com.tora;

import com.tora.entities.Nr;
import com.tora.entities.Operand;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test for simple App.
 */
public class AppTest
{
    @Test
    public void TestNr(){
        Nr number = new Nr(10);
        assertEquals(10,number.getElement());
    }

    @Test
    public void TestGoodOperand(){
        Operand op = new Operand("sqrt");
        assertEquals("sqrt",op.getOperand());
    }

    @Test
    public void TestBadOperand(){
        Operand op = new Operand(".");
        assertNull(op.calculate());
    }

    @Test
    public void TestAdd1(){
        Nr nr1 = new Nr(15.4);
        Nr nr2 = new Nr(6.09);
        Operand operation = new Operand("+");
        operation.setNr1(nr1);
        operation.setNr2(nr2);
        Nr rez = operation.calculate();
        assertEquals(21.49,rez.getElement());
    }

    @Test
    public void TestAdd2(){
        Nr nr1 = new Nr(-5);
        Nr nr2 = new Nr(5);
        Operand operation = new Operand("+");
        operation.setNr1(nr1);
        operation.setNr2(nr2);
        Nr rez = operation.calculate();
        assertEquals(0,rez.getElement());
    }

    @Test
    public void TestSubtract1(){
        Nr nr1 = new Nr(57);
        Nr nr2 = new Nr(60.3);
        Operand operation = new Operand("-");
        operation.setNr1(nr1);
        operation.setNr2(nr2);
        Nr rez = operation.calculate();
        assertEquals(-3.3,rez.getElement());
    }

    @Test
    public void TestSubtract2(){
        Nr nr1 = new Nr(18.74);
        Nr nr2 = new Nr(9.05);
        Operand operation = new Operand("-");
        operation.setNr1(nr1);
        operation.setNr2(nr2);
        Nr rez = operation.calculate();
        assertEquals(9.69,rez.getElement());
    }

    @Test
    public void TestMultiply1(){
        Nr nr1 = new Nr(8);
        Nr nr2 = new Nr(1.5);
        Operand operation = new Operand("*");
        operation.setNr1(nr1);
        operation.setNr2(nr2);
        Nr rez = operation.calculate();
        assertEquals(12,rez.getElement());
    }

    @Test
    public void TestMultiply2(){
        Nr nr1 = new Nr(10);
        Nr nr2 = new Nr(143.88);
        Operand operation = new Operand("*");
        operation.setNr1(nr1);
        operation.setNr2(nr2);
        Nr rez = operation.calculate();
        assertEquals(1438.8,rez.getElement());
    }

    @Test
    public void TestDivide1(){
        Nr nr1 = new Nr(40);
        Nr nr2 = new Nr(3);
        Operand operation = new Operand("/");
        operation.setNr1(nr1);
        operation.setNr2(nr2);
        Nr rez = operation.calculate();
        assertEquals(13.33333,rez.getElement());
    }

    @Test
    public void TestDivide2(){
        Nr nr1 = new Nr(30);
        Nr nr2 = new Nr(0.5);
        Operand operation = new Operand("/");
        operation.setNr1(nr1);
        operation.setNr2(nr2);
        Nr rez = operation.calculate();
        assertEquals(60,rez.getElement());
    }

    @Test
    public void TestMin1(){
        Nr nr1 = new Nr(11);
        Nr nr2 = new Nr(19.07);
        Operand operation = new Operand("min");
        operation.setNr1(nr1);
        operation.setNr2(nr2);
        Nr rez = operation.calculate();
        assertEquals(11,rez.getElement());
    }

    @Test
    public void TestMin2(){
        Nr nr1 = new Nr(26);
        Nr nr2 = new Nr(9.12);
        Operand operation = new Operand("min");
        operation.setNr1(nr1);
        operation.setNr2(nr2);
        Nr rez = operation.calculate();
        assertEquals(9.12,rez.getElement());
    }

    @Test
    public void TestMax1(){
        Nr nr1 = new Nr(11);
        Nr nr2 = new Nr(19.07);
        Operand operation = new Operand("max");
        operation.setNr1(nr1);
        operation.setNr2(nr2);
        Nr rez = operation.calculate();
        assertEquals(19.07,rez.getElement());
    }

    @Test
    public void TestMax2(){
        Nr nr1 = new Nr(26);
        Nr nr2 = new Nr(9.12);
        Operand operation = new Operand("max");
        operation.setNr1(nr1);
        operation.setNr2(nr2);
        Nr rez = operation.calculate();
        assertEquals(26,rez.getElement());
    }

    @Test
    public void TestSqrt1(){
        Nr number = new Nr(36);
        Operand operation = new Operand("sqrt");
        operation.setNr1(number);
        Nr rez = operation.calculate();
        assertEquals(6,rez.getElement());
    }

    @Test
    public void TestSqrt2(){
        Nr number = new Nr(49);
        Operand operation = new Operand("sqrt");
        operation.setNr1(number);
        Nr rez = operation.calculate();
        assertEquals(7,rez.getElement());
    }
}
