package com.tora.entities;

public class Nr{

    private double element;

    public Nr(double element) {
        this.element = element;
    }

    public double getElement() {
        return element;
    }

    public void setElement(double element) {
        this.element = element;
    }
}
