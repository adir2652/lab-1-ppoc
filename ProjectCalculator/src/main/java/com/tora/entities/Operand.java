package com.tora.entities;

import java.util.Formatter;

public class Operand {

    private Nr nr1;
    private Nr nr2;

    private String operand;

    public Operand(String operand) {
        this.operand = operand;
    }

    public void setNr1(Nr nr1) {
        this.nr1 = nr1;
    }

    public void setNr2(Nr nr2) {
        this.nr2 = nr2;
    }

    private Nr add() {
        double a = nr1.getElement();
        double b = nr2.getElement();
        double rez = a + b;
        Formatter f = new Formatter();
        f.format("%.5f", rez);
        String format = String.valueOf(f).replace(',', '.');
        rez = Double.parseDouble(format);
        return new Nr(rez);
    }

    private Nr minus() {
        double a = nr1.getElement();
        double b = nr2.getElement();
        double rez = a - b;
        Formatter f = new Formatter();
        f.format("%.5f", rez);
        String format = String.valueOf(f).replace(',', '.');
        rez = Double.parseDouble(format);
        return new Nr(rez);
    }

    private Nr multiply() {
        double a = nr1.getElement();
        double b = nr2.getElement();
        double rez = a * b;
        Formatter f = new Formatter();
        f.format("%.5f", rez);
        String format = String.valueOf(f).replace(',', '.');
        rez = Double.parseDouble(format);
        return new Nr(rez);
    }

    private Nr divide() {
        double a = nr1.getElement();
        double b = nr2.getElement();
        double rez = a / b;
        Formatter f = new Formatter();
        f.format("%.5f", rez);
        String format = String.valueOf(f).replace(',', '.');
        rez = Double.parseDouble(format);
        return new Nr(rez);
    }

    private Nr minim() {
        double a = nr1.getElement();
        double b = nr2.getElement();
        if (a < b) return nr1;
        return nr2;
    }

    private Nr maxim() {
        double a = nr1.getElement();
        double b = nr2.getElement();
        if (a > b) return nr1;
        return nr2;
    }

    private Nr sqrt() {
        double a = nr1.getElement();
        double rez = Math.sqrt(a);
        Formatter f = new Formatter();
        f.format("%.5f", rez);
        String format = String.valueOf(f).replace(',', '.');
        rez = Double.parseDouble(format);
        return new Nr(rez);
    }

    public String getOperand() {
        return operand;
    }

    public void setOperand(String operand) {
        this.operand = operand;
    }

    public Nr calculate() {
        switch (getOperand()) {
            case "+": {
                return add();
            }
            case "-": {
                return minus();
            }
            case "*": {
                return multiply();
            }
            case "/": {
                return divide();
            }
            case "min": {
                return minim();
            }
            case "max": {
                return maxim();
            }
            case "sqrt": {
                return sqrt();
            }
        }
        return null;
    }
}
