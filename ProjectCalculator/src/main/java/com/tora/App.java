package com.tora;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tora.entities.*;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        while (true) {
            System.out.println("1. Calculate\n2. Exit\n");
            System.out.print("Your command: ");

            Scanner in = new Scanner(System.in);
            String cmd = in.nextLine();

            if (cmd.equals("1")) {
                System.out.println("CALCULATOR");
                Pattern pattern1 = Pattern.compile("[1-9][0-9]*\\.?[0-9]*\\s[+\\-*/]\\s[1-9][0-9]*\\.?[0-9]*");
                Pattern pattern2 = Pattern.compile("(min|max)\\s[1-9][0-9]*\\.?[0-9]*\\s[1-9][0-9]*\\.?[0-9]*");
                Pattern pattern3 = Pattern.compile("sqrt\\s[1-9][0-9]*\\.?[0-9]*");

                String operation = in.nextLine();
                Matcher matcher = pattern1.matcher(operation);
                boolean matchFound = matcher.find();
                int okPattern = 0;
                if (matchFound) {
                    okPattern = 1;
                } else {
                    matcher = pattern2.matcher(operation);
                    matchFound = matcher.find();
                    if (matchFound) {
                        okPattern = 2;
                    } else {
                        matcher = pattern3.matcher(operation);
                        matchFound = matcher.find();
                        if (matchFound) {
                            okPattern = 3;
                        }
                    }
                }
                if (okPattern > 0) {
                    int index = 0;
                    List<String> elements = new ArrayList<>();
                    String substring = "";
                    while (index < operation.length()) {
                        if (operation.charAt(index) != ' ') {
                            substring += operation.charAt(index);
                        } else {
                            elements.add(substring);
                            substring = "";
                        }
                        index++;
                    }
                    elements.add(substring);
                    if (okPattern == 1) {
                        double number1 = Double.parseDouble(elements.get(0));
                        double number2 = Double.parseDouble(elements.get(2));
                        Nr nr1 = new Nr(number1);
                        Nr nr2 = new Nr(number2);
                        Operand op = new Operand(elements.get(1));
                        op.setNr1(nr1);
                        op.setNr2(nr2);
                        Nr rez = op.calculate();
                        System.out.print("------------------\n");
                        System.out.println(String.valueOf(rez.getElement()) + "\n");
                    }
                    if (okPattern == 2) {
                        double number1 = Double.parseDouble(elements.get(1));
                        double number2 = Double.parseDouble(elements.get(2));
                        Nr nr1 = new Nr(number1);
                        Nr nr2 = new Nr(number2);
                        Operand op = new Operand(elements.get(0));
                        op.setNr1(nr1);
                        op.setNr2(nr2);
                        Nr rez = op.calculate();
                        System.out.print("------------------\n");
                        System.out.println(String.valueOf(rez.getElement()) + "\n");
                    }
                    if (okPattern == 3) {
                        double number1 = Double.parseDouble(elements.get(1));
                        Nr nr1 = new Nr(number1);
                        Operand op = new Operand(elements.get(0));
                        op.setNr1(nr1);
                        Nr rez = op.calculate();
                        System.out.print("------------------\n");
                        System.out.println(String.valueOf(rez.getElement()) + "\n");
                    }
                } else {
                    System.out.println("NO PATTERN MATCHED\n");
                }
            } else if (cmd.equals("2")) {
                break;
            } else {
                System.out.println("INVALID COMMAND\n");
            }
        }
    }
}
